/**
 * @file
 * JavaScript behaviors for Webform JavaScript Setting element.
 */
"use strict";

((Drupal, once) => {

  /**
   * Set JavaScript setting element's value.
   *
   * @param {HTMLElement} element
   *   The JavaScript setting element.
   *
   * @returns {boolean}
   *   TRUE if the element's value is set.
   */
  function setValue(element) {
    try {
      // Get the JavaScript settings using the dot notation.
      const setting = element.getAttribute('data-webform-javascript-setting');
      // Convert a JavaScript string in dot notation into an object reference.
      // @see https://stackoverflow.com/questions/6393943/convert-a-javascript-string-in-dot-notation-into-an-object-reference
      const value = setting.split('.').reduce(
        function (object, property) {
          // If the property is a method (aka function) call it.
          if (typeof object[property] === 'function') {
            return object[property]();
          }
          else {
            return object[property];
          }
        }, window);

      // Set the hidden element's value.
      if (value) {
        element.value = value;
        return true;
      }
      return false;
    }
    catch (e) {
      return false;
    }
  }

  /**
   * Schema.org form behaviors.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformJavaScriptSetting = {
    attach: function attach(context) {
      once('webform-javascript-setting', 'input[data-webform-javascript-setting]', context)
        .forEach((element) => {
          // Skip if the element's value is already set.
          if (element.value) {
            return;
          }

          // Attempt to set the value and return.
          if (setValue(element)) {
            return;
          }

          // If the value is not set, keep trying to set the value every second.
          const interval = setInterval(function () {
            if (setValue(element)) {
              clearInterval(interval);
            }
          }, 2000);
        });
    }
  };
})(Drupal, once);
