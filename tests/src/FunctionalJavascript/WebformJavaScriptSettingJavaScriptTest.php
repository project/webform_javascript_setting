<?php

declare(strict_types=1);

namespace Drupal\Tests\webform_setting_javascript\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests webform javascript setting element.
 *
 * @group webform_javascript_setting
 */
class WebformJavaScriptSettingJavaScriptTest extends WebDriverTestBase {

  /**
   * Set default theme to classy.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = ['webform_javascript_setting_test'];

  /**
   * Test javascript setting element.
   */
  public function testJavaScriptSettingElement() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->rootUser);

    // Check JavaScript settings field is populated.
    $this->drupalGet('/webform/test_webform_javascript_setting/');
    $assert_session->hiddenFieldExists('webform_javascript_setting_user_id');
    $assert_session->hiddenFieldValueEquals('webform_javascript_setting_user_id', '1');
    $assert_session->hiddenFieldExists('webform_javascript_setting_get');
    $assert_session->hiddenFieldValueEquals('webform_javascript_setting_get', '{some_value}');
  }

}
