<?php

namespace Drupal\webform_javascript_setting\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\TextBase;
use Drupal\webform\WebformInterface;

/**
 * Provides a 'webform_javascript_setting' element.
 *
 * @WebformElement(
 *   id = "webform_javascript_setting",
 *   label = @Translation("JavaScript Setting"),
 *   description = @Translation("Provides a form element to capture and store a JavaScript setting in a 'hidden' input element."),
 *   category = @Translation("Advanced elements"),
 * )
 */
class WebformJavaScriptSetting extends TextBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'title' => '',
      'javascript_setting' => '',
    ];
  }

  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function preview() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTestValues(array $element, WebformInterface $webform, array $options = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\webform\WebformThirdPartySettingsManagerInterface $third_party_settings_manager */
    $third_party_settings_manager = \Drupal::service('webform.third_party_settings_manager');
    $settings = $third_party_settings_manager->getThirdPartySetting('webform_javascript_setting', 'settings');

    $options = [];
    foreach ($settings as $name => $setting) {
      $t_args = [
        '@label' => $setting['label'],
        '@setting' => $setting['setting'],
      ];
      $options[$name] = $this->t('@label (@setting)', $t_args);
    }
    $form['element']['javascript_setting'] = [
      '#type' => 'select',
      '#title' => $this->t('JavaScript setting'),
      '#description' => $this->t('The JavaScript setting to populate the value of the webform element.'),
      '#required' => TRUE,
      '#options' => $options,
    ];

    return $form;
  }

}
