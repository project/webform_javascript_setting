<?php

namespace Drupal\webform_javascript_setting\Element;

use Drupal\Core\Render\Element\Hidden;

/**
 * Adds a Webform element to store a JavaScript setting in a hidden input.
 *
 * @FormElement("webform_javascript_setting")
 */
class WebformJavaScriptSetting extends Hidden {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#javascript_setting'] = '';
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderHidden($element) {
    $element = parent::preRenderHidden($element);

    /** @var \Drupal\webform\WebformThirdPartySettingsManagerInterface $third_party_settings_manager */
    $third_party_settings_manager = \Drupal::service('webform.third_party_settings_manager');
    $settings = $third_party_settings_manager->getThirdPartySetting('webform_javascript_setting', 'settings');
    $javascript_setting = $settings[$element['#javascript_setting']] ?? [];
    $element['#attributes']['data-webform-javascript-setting'] = $javascript_setting['setting'] ?? $element['#javascript_setting'];
    $element['#attached']['library'][] = 'webform_javascript_setting/webform_javascript_setting';
    return $element;
  }

}
